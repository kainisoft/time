import React from 'react';
import { StyleSheet, View, TimePickerAndroid, Button } from 'react-native';

export default class App extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Button
          title={'TimePickerAndroid'}
          onPress={async () => {
          const {action: a2, hour, minute} = await TimePickerAndroid.open({
            hour: (new Date().getHours()),
            minute: (new Date().getMinutes()),
            mode: 'clock',
          });

          if (a2 !== TimePickerAndroid.dismissedAction) {
            console.log(hour, minute);
          }
        }}>
        </Button>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
